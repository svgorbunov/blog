using System;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Threading; 

namespace SVGorbunov.Blog.Rake
{
	public class TimeStoreService : ServiceBase
	{
		private Timer timer;
		
		private String storePath;
	
		public TimeStoreService()
		{
			this.ServiceName = "Служба хранения времени";
			this.CanStop = true;
			this.CanPauseAndContinue = true;
			this.AutoLog = true;
			
			this.storePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "store.txt");
		}
		
		protected override void OnStart(String[] args)
		{
			this.timer = new Timer(TimerElapsed, null, 1000, 5000);		
		}
		
		private void TimerElapsed(Object state)
		{
			using (var sw = new StreamWriter(storePath, true))
			{
				sw.WriteLine(DateTime.Now.ToString());
			}
		}
	}
}