using System.ServiceProcess;

namespace SVGorbunov.Blog.Rake
{
	public class Program
	{
		public static void Main()
		{
			ServiceBase.Run(new TimeStoreService());
		}
	}
}