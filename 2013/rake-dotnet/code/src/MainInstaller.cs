using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace SVGorbunov.Blog.Rake
{
	[RunInstaller(true)]
	public class ProjectInstaller : Installer
	{
		private ServiceInstaller serviceInstaller;

		private ServiceProcessInstaller processInstaller;

		public ProjectInstaller()
		{
			this.processInstaller = new ServiceProcessInstaller
			{
				Account = ServiceAccount.NetworkService
			};

			this.serviceInstaller = new ServiceInstaller
			{
				StartType = ServiceStartMode.Automatic,
				ServiceName = "Служба хранения времени"
			};

			Installers.Add(this.serviceInstaller);
			Installers.Add(this.processInstaller);
		}
	}
}